import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ArticleService} from '../../service/article.service';
import {ArticleFullInfoModel} from '../../model/articleFullInfo.model';
import {UserFullInfoModel} from '../../model/userFullInfo.model';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.css']
})
export class AccueilComponent implements OnInit {

  search: string;
  registerInformations: UserFullInfoModel;
  articlesList: ArticleFullInfoModel [];
  showCreateAccount = false;
  showUserArticles = false;
  showRegister = false;
  showNewArticle = false;
  showAll = true;

  constructor(private service: ArticleService) {
  }

  ngOnInit() {
  }

  homepage() {
    this.showCreateAccount = false;
    this.showNewArticle = false;
    this.showRegister = false;
    this.showUserArticles = false;
    this.showAll = true;
  }

  searchFromTitle() {
    if (this.search !== null && this.search !== '') {
      this.service.getArticlesWithSearchBar(this.search).subscribe(articles => {
        this.articlesList = articles;
      });
    } else {
      this.service.getAllPublishedArticles().subscribe(articles => this.articlesList = articles);
    }
  }

  createAccount() {
    this.showCreateAccount = true;
    this.showAll = false;
  }

  goRegister() {
    this.showAll = false;
    this.showRegister = true;
  }

  showCreateArticlePage() {
this.showAll = false;
this.showNewArticle = true;
  }

  showMyArticles() {
    this.showAll = false;
    this.showUserArticles = true;
  }

  onreturnHomepage(event: boolean) {
    if (event === true) {
      this.showCreateAccount = false;
      this.showRegister = false;
      this.showAll = true;
      this.showNewArticle = false;
    }
  }

  onRegisterInfo(register: UserFullInfoModel) {
    this.registerInformations = register;
  }

}
