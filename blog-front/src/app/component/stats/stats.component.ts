import {Component, OnInit} from '@angular/core';
import {ArticleService} from '../../service/article.service';
import {ArticleFullInfoModel} from '../../model/articleFullInfo.model';

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.css']
})
export class StatsComponent implements OnInit {

  articles: ArticleFullInfoModel[] = [];
  articlesPublish: ArticleFullInfoModel[] = [];

  constructor(private service: ArticleService) {
  }

  ngOnInit() {
    this.service.getAllArticles().subscribe(articles => this.articles = articles);
    this.service.getAllPublishedArticles().subscribe(articles => this.articlesPublish = articles);
  }

}
