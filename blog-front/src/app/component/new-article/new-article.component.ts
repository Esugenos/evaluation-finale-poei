import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {UserFullInfoModel} from '../../model/userFullInfo.model';
import {ArticleFullInfoModel} from '../../model/articleFullInfo.model';
import {ArticleService} from '../../service/article.service';
import {BodyArticleCreationModel} from '../../model/bodyArticleCreation.model';
import {BodyJoinArticleToUserModel} from '../../model/bodyJoinArticleToUser.model';
import {UserService} from '../../service/user.service';

@Component({
  selector: 'app-new-article',
  templateUrl: './new-article.component.html',
  styleUrls: ['./new-article.component.css']
})
export class NewArticleComponent implements OnInit {

  title: string;
  category: string;
  resume: string;
  content: string;
  disabled = true;
  showMessage = false;
  showInput = true;
  showAlert = false;
  response: ArticleFullInfoModel;
  return = false;
  alert: string;

  @Output() homepage = new EventEmitter();
  @Input() infoRegister: UserFullInfoModel;

  constructor(private service: ArticleService) {
  }

  ngOnInit() {

  }

  change() {
    if (this.content == null || this.content === '' && this.resume == null ||
      this.resume === '' || this.resume.length > 50 && this.title != null && this.title !== '' && this.category != null && this.category !== '') {
      this.disabled = true;
    } else {
      this.disabled = false;
    }
    if (this.resume.length > 50) {
      this.alert = 'Votre résumé dépasse 50 caractères';
      this.showAlert = true;
    } else {
      this.alert = '';
      this.showAlert = false;
    }
  }


  chooseCategory(cat: string) {
    this.category = cat;
  }

  createArticle() {
    const newArticle = new BodyArticleCreationModel(this.title, this.category, this.resume, this.content);
    this.service.postCreateArticle(newArticle).subscribe(reponse => {
      this.response = reponse;
    });


    this.showInput = false;
    this.showMessage = true;
  }

  returnHomepage() {

    console.log(this.response);
    console.log(this.infoRegister);
    console.log(this.infoRegister.id);
    console.log(this.response.id);
    const body = new BodyJoinArticleToUserModel(this.infoRegister.id, this.response.id);
    this.service.putJoinArticleToUser(body).subscribe();
    this.return = true;
    this.homepage.emit(this.return);
  }
}
