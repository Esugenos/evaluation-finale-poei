import {Component, Input, OnInit} from '@angular/core';
import {ArticleFullInfoModel} from '../../model/articleFullInfo.model';
import {ArticleService} from '../../service/article.service';
import {UserService} from '../../service/user.service';
import {UserFullInfoModel} from '../../model/userFullInfo.model';
import {BodyUpdatePublishModel} from '../../model/bodyUpdatePublish.model';
import {BodyUpdateContentModel} from '../../model/bodyUpdateContent.model';
import {ResponseRepresentationModel} from '../../model/responseRepresentation.model';

@Component({
  selector: 'app-user-articles',
  templateUrl: './user-articles.component.html',
  styleUrls: ['./user-articles.component.css']
})
export class UserArticlesComponent implements OnInit {

  @Input() articles: ArticleFullInfoModel [] = [];
  showButtonPublish = true;
  showNewContent = false;
  showArticlePublish = false;
  showContentChange = false;
  disabled = false;
  MessageButtonContent = 'Changer un contenu';
  @Input() User: UserFullInfoModel;
  responsePublish: ResponseRepresentationModel;
  responseContent: ResponseRepresentationModel;
  newContent: string;


  constructor(private service: UserService, private serviceArticle: ArticleService) {
  }

  ngOnInit() {
    this.service.getYourArticles(this.User.id).subscribe(articles => {
      this.articles = articles;
    });
    this.articles.forEach(article => {
      console.log(article.status);
      if (article.status === 'PUBLISHED') {
        this.showButtonPublish = false;
      }
    });
  }

  publish(id: string) {
    const body = new BodyUpdatePublishModel(id, 'PUBLISHED');
    this.serviceArticle.putPublishArticle(body).subscribe(response => this.responsePublish = response);
    this.showArticlePublish = true;
    this.showButtonPublish = false;
  }

  changeContent(id: string) {
    const body = new BodyUpdateContentModel(id, this.newContent);
    this.serviceArticle.putUpdateContent(body).subscribe(response => {
      this.responseContent = response;
      this.service.getYourArticles(this.User.id).subscribe(articles => {
        this.articles = articles;
      });
    });
    this.showContentChange = true;
    this.showNewContent = false;
    this.MessageButtonContent = 'Changer un contenu';
    this.disabled = false;
  }

  content() {
    this.showNewContent = true;
    this.disabled = true;
    this.MessageButtonContent = 'Cliquez sur valider sous l\'article souhaité';
  }
}



