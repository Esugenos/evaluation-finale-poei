import {Component, Input, OnInit} from '@angular/core';
import {ArticleService} from '../../service/article.service';
import {ArticleFullInfoModel} from '../../model/articleFullInfo.model';

@Component({
  selector: 'app-list-article',
  templateUrl: './list-article.component.html',
  styleUrls: ['./list-article.component.css']
})
export class ListArticleComponent implements OnInit {

  @Input() articles: ArticleFullInfoModel [] = [];
  showArticlesForOneCategory = true;

  constructor(private service: ArticleService) {
  }

  ngOnInit() {
    this.service.getAllPublishedArticles().subscribe(articles => {
      this.articles = articles;
    });
  }

  chooseCategory(category: string) {
    this.service.getArticlesByCategory(category).subscribe(articles => this.articles = articles);
  }
}
