import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {UserFullInfoModel} from '../../model/userFullInfo.model';
import {UserService} from '../../service/user.service';
import {BodyUserCreationModel} from '../../model/bodyUserCreation.model';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  pseudo: string;
  password: string;
  password2: string;
  disabled = true;
  showWarnPassword = false;
  user: UserFullInfoModel;
  usersInfos: UserFullInfoModel [];
  return = false;
  @Output() homepage = new EventEmitter();
  @Output() infoRegister = new EventEmitter();

  constructor(private service: UserService) {
  }

  ngOnInit() {
    this.service.getAllUsers().subscribe(users => this.usersInfos = users);
  }

  change() {
    if (this.password2 == null || this.password2 === '' || this.password == null || this.password === '') {
      this.showWarnPassword = false;
      this.disabled = true;
    } else if (this.password2 !== this.password) {
      this.showWarnPassword = true;
      this.disabled = true;
    } else if (this.password2 === this.password) {
      this.showWarnPassword = false;
      if (this.pseudo != null && this.pseudo !== '') {
        this.disabled = false;
      }
    }
  }

  registerMe() {
    this.usersInfos.forEach(userInfos1 => {
      if (this.pseudo === userInfos1.pseudo) {
        if (this.password === userInfos1.password) {
          this.user = userInfos1;
          this.return = true;
        }
      }
    });

    this.homepage.emit(this.return);
    this.infoRegister.emit(this.user);
  }
}
