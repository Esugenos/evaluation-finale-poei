import {Component, Input, OnInit} from '@angular/core';
import {ArticleFullInfoModel} from '../../model/articleFullInfo.model';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {
  @ Input() article: ArticleFullInfoModel;
  @ Input() title: string;
  @ Input() resume: string;
  @ Input() category: string;
  @ Input() content: string;
  showContent = false;
  constructor() { }

  ngOnInit() {
  }
showAllArticle() {
    this.showContent = true;
}
reduceContent() {
    this.showContent = false;
}
}
