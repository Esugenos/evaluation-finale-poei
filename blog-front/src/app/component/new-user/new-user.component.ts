import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {UserService} from '../../service/user.service';
import {UserFullInfoModel} from '../../model/userFullInfo.model';
import {BodyUserCreationModel} from '../../model/bodyUserCreation.model';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css']
})
export class NewUserComponent implements OnInit {
  pseudo: string;
  mail: string;
  password: string;
  password2: string;
  disabled = true;
  showWarnPassword = false;
  showMessage = false;
  showInput = true;
  ResponseServer: UserFullInfoModel;
  return = false;
  @Output() homepage = new EventEmitter();
  @Output() infoRegister = new EventEmitter();

  constructor(private service: UserService) {
  }

  ngOnInit() {
  }

  change() {
    if (this.password2 == null || this.password2 === '' || this.password == null || this.password === '') {
      this.showWarnPassword = false;
      this.disabled = true;
    } else if (this.password2 !== this.password) {
      this.showWarnPassword = true;
      this.disabled = true;
    } else if (this.password2 === this.password) {
      this.showWarnPassword = false;
      if (this.pseudo != null && this.pseudo !== '') {
        if (this.mail != null && this.mail !== '') {
          this.disabled = false;
        }
      }
    }
  }

  createUser() {
    const newUser = new BodyUserCreationModel(this.pseudo, this.mail, this.password);
    this.service.postCreateUser(newUser).subscribe(reponse => {
      this.ResponseServer = reponse;
    });
    this.showInput = false;
    this.showMessage = true;


  }

  returnHomepage() {
    this.return = true;
    this.homepage.emit(this.return);
    this.infoRegister.emit(this.ResponseServer);

  }
}
