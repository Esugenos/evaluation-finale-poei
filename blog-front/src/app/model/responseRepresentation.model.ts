export class ResponseRepresentationModel {
  response: string;

  constructor(response: string) {
    this.response = response;
  }
}
