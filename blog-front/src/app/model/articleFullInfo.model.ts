export class ArticleFullInfoModel {

  id: string;
  title: string;
  category: string;
  status: string;
  resume: string;
  content: string;

  constructor(id: string, title: string, category: string, status: string, resume: string, content: string) {
    this.id = id;
    this.title = title;
    this.category = category;
    this.status = status;
    this.resume = resume;
    this.content = content;

  }
}
