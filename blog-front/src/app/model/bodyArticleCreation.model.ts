export class BodyArticleCreationModel {

  title: string;
  category: string;
  resume: string;
  content: string;

  constructor(title: string, category: string, resume: string, content: string) {
    this.title = title;
    this.category = category;
    this.resume = resume;
    this.content = content;

  }
}
