import {ArticleFullInfoModel} from './articleFullInfo.model';

export class BodyUserCreationModel {


  pseudo: string;
  mail: string;
  password: string;

  constructor(pseudo: string, mail: string, password: string) {
    this.pseudo = pseudo;
    this.mail = mail;
    this.password = password;

  }
}
