import {ArticleFullInfoModel} from './articleFullInfo.model';

export class UserFullInfoModel {

  id: string;
  pseudo: string;
  mail: string;
  password: string;
  articles: ArticleFullInfoModel[];

  constructor(id: string, pseudo: string, mail: string, password: string, articles: ArticleFullInfoModel[]) {
    this.id = id;
    this.pseudo = pseudo;
    this.mail = mail;
    this.password = password;
    this.articles = articles;
  }
}
