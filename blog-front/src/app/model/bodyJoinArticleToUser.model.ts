export class BodyJoinArticleToUserModel {
  idUser: string;
  idArticle: string;

  constructor(idUser: string, idArticle: string) {
    this.idUser = idUser;
    this.idArticle = idArticle;
  }
}
