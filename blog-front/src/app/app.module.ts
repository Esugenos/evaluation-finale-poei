import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AccueilComponent } from './component/accueil/accueil.component';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ArticleComponent } from './component/article/article.component';
import { ListArticleComponent } from './component/list-article/list-article.component';
import {ArticleService} from './service/article.service';
import { NewUserComponent } from './component/new-user/new-user.component';
import {UserService} from './service/user.service';
import { UserArticlesComponent } from './component/user-articles/user-articles.component';
import { RegisterComponent } from './component/register/register.component';
import { NewArticleComponent } from './component/new-article/new-article.component';
import { StatsComponent } from './component/stats/stats.component';


const appRoutes: Routes = [
  { path: '', component: AccueilComponent },
  { path: 'statistiques', component: StatsComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    AccueilComponent,
    ArticleComponent,
    ListArticleComponent,
    NewUserComponent,
    UserArticlesComponent,
    RegisterComponent,
    NewArticleComponent,
    StatsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forRoot(
      appRoutes),
    FormsModule
  ],
  providers: [ArticleService, UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
