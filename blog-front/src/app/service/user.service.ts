import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {ArticleFullInfoModel} from '../model/articleFullInfo.model';
import {UserFullInfoModel} from '../model/userFullInfo.model';
import {BodyUserCreationModel} from '../model/bodyUserCreation.model';

@Injectable()
export class UserService {

  constructor(private http: HttpClient) {
  }

  getAllUsers(): Observable<UserFullInfoModel[]> {
    return this.http.get<UserFullInfoModel[]>('http://localhost:8088/users');
  }

  postCreateUser(body: BodyUserCreationModel): Observable<UserFullInfoModel> {
    return this.http.post<UserFullInfoModel>('http://localhost:8088/users/create', body);
  }
  getUserById(id: string): Observable<UserFullInfoModel> {
    return this.http.get<UserFullInfoModel>(`http://localhost:8088/users/${id}`);
  }

  getYourArticles(id: string): Observable<ArticleFullInfoModel[]> {
    return this.http.get<ArticleFullInfoModel[]>(`http://localhost:8088/users/your_articles/${id}`);
  }

  deleteUser(id: string): Observable<any> {
    return this.http.delete(`http://localhost:8088/users/delete/${id}`);
  }
}
