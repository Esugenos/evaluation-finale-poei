import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {ArticleFullInfoModel} from '../model/articleFullInfo.model';
import {BodyArticleCreationModel} from '../model/bodyArticleCreation.model';
import {BodyUpdateContentModel} from '../model/bodyUpdateContent.model';
import {BodyUpdatePublishModel} from '../model/bodyUpdatePublish.model';
import {BodyJoinArticleToUserModel} from '../model/bodyJoinArticleToUser.model';
import {ResponseRepresentationModel} from '../model/responseRepresentation.model';
import {BodySearchByCategoryModel} from '../model/bodySearchByCategory.model';

@Injectable()
export class ArticleService {

  constructor(private http: HttpClient) {
  }

  getAllArticles(): Observable<ArticleFullInfoModel[]> {
    return this.http.get<ArticleFullInfoModel[]>('http://localhost:8088/articles');
  }

  getAllDraftArticles(): Observable<ArticleFullInfoModel[]> {
    return this.http.get<ArticleFullInfoModel[]>('http://localhost:8088/articles/draft');
  }

  getAllPublishedArticles(): Observable<ArticleFullInfoModel[]> {
    return this.http.get<ArticleFullInfoModel[]>('http://localhost:8088/articles/published');
  }
  getArticlesByCategory(category: string): Observable<ArticleFullInfoModel[]>{
    return this.http.get<ArticleFullInfoModel[]>(`http://localhost:8088/articles/category/${category}`);
  }

  getArticlesWithSearchBar(search: string): Observable<ArticleFullInfoModel[]> {
    return this.http.get<ArticleFullInfoModel[]>(`http://localhost:8088/articles/search/${search}`);
  }

  postCreateArticle(body: BodyArticleCreationModel): Observable<ArticleFullInfoModel> {
    return this.http.post<ArticleFullInfoModel>('http://localhost:8088/articles/create', body);
  }
  getArticleById(id: string): Observable<ArticleFullInfoModel> {
    return this.http.get<ArticleFullInfoModel>(`http://localhost:8088/articles/${id}`);
  }

  deleteArticle(id: string): Observable<object> {
    return this.http.delete(`http://localhost:8088/articles/delete/${id}`);
  }
  putUpdateContent(body: BodyUpdateContentModel): Observable<ResponseRepresentationModel> {
    return this.http.put<ResponseRepresentationModel>('http://localhost:8088/articles/updatecontent', body);
  }
  putPublishArticle(body: BodyUpdatePublishModel): Observable<ResponseRepresentationModel> {
    return this.http.put<ResponseRepresentationModel>('http://localhost:8088/articles/publish', body);
  }
  putJoinArticleToUser(body: BodyJoinArticleToUserModel): Observable<any> {
    return this.http.put('http://localhost:8088/articles/jointouser', body);
  }
}
