package com.zenika.academy.blog.Service;

import com.zenika.academy.blog.Controller.Representation.ChangeContentRepresentation;
import com.zenika.academy.blog.Controller.Representation.ChangeStatusRepresentation;
import com.zenika.academy.blog.Repository.ArticleRepository;
import com.zenika.academy.blog.Repository.Domain.Article;
import com.zenika.academy.blog.Repository.Domain.Enum.Category;
import com.zenika.academy.blog.Repository.Domain.Enum.StatusArticle;
import com.zenika.academy.blog.Repository.Domain.User;
import com.zenika.academy.blog.Repository.UserRepository;
import com.zenika.academy.blog.Service.Factory.ArticleFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class ArticleServiceTest {

    @Test
    void updateArticle() {
        ArticleRepository repository = Mockito.mock(ArticleRepository.class);
        UserRepository userRepository = Mockito.mock(UserRepository.class);
        ChangeContentRepresentation changeContent = new ChangeContentRepresentation("1","TestContentChange");
        ArticleFactory factory = new ArticleFactory();
        ArticleService service = new ArticleService(repository,userRepository,factory);
        Article article = new Article("1","Test", Category.JAVA,StatusArticle.DRAFT,"TestContent","TestResume");
        Mockito.when(repository.findById("1")).thenReturn(Optional.of(article));

        Assertions.assertEquals(Optional.of(article),service.updateArticle(changeContent));
    }

    @Test
    void getAllArticles() {
        ArticleRepository repository = Mockito.mock(ArticleRepository.class);
        UserRepository userRepository = Mockito.mock(UserRepository.class);
        ArticleFactory factory = new ArticleFactory();
        ArticleService service = new ArticleService(repository,userRepository,factory);
        Article article = factory.generatenewArticle("Test", Category.JAVA,"TestContent","TestResume");
        Article article2 = factory.generatenewArticle("Autretest", Category.JAVA,"TestContent2","TestResume2");
        article.setStatus(StatusArticle.PUBLISHED);
        List<Article> articles = new ArrayList<>(List.of(article, article2));
        List<Article> expected = new ArrayList<>(List.of(article, article2));
        Mockito.when(repository.findAll()).thenReturn(articles);

        Assertions.assertEquals(expected,service.getAllArticles());
    }

    @Test
    void getDraftArticles() {
        ArticleRepository repository = Mockito.mock(ArticleRepository.class);
        UserRepository userRepository = Mockito.mock(UserRepository.class);
        ArticleFactory factory = new ArticleFactory();
        ArticleService service = new ArticleService(repository,userRepository,factory);
        Article article = factory.generatenewArticle("Test", Category.JAVA,"TestContent","TestResume");
        Article article2 = factory.generatenewArticle("AutreTest", Category.JAVA,"TestContent2","TestResume2");
        article.setStatus(StatusArticle.PUBLISHED);
        List<Article> articles = new ArrayList<>(List.of(article, article2));
        List<Article> expected = new ArrayList<>(List.of(article2));
        Mockito.when(repository.findAll()).thenReturn(articles);

        Assertions.assertEquals(expected,service.getDraftArticles());
    }

    @Test
    void getPublishedArticles() {
        ArticleRepository repository = Mockito.mock(ArticleRepository.class);
        UserRepository userRepository = Mockito.mock(UserRepository.class);
        ArticleFactory factory = new ArticleFactory();
        ArticleService service = new ArticleService(repository,userRepository,factory);
        Article article = factory.generatenewArticle("Test", Category.JAVA,"TestContent","TestResume");
        Article article2 = factory.generatenewArticle("AutreTest", Category.JAVA,"TestContent2","TestResume2");
        article.setStatus(StatusArticle.PUBLISHED);
        List<Article> articles = new ArrayList<>(List.of(article, article2));
        List<Article> expected = new ArrayList<>(List.of(article));
        Mockito.when(repository.findAll()).thenReturn(articles);

        Assertions.assertEquals(expected,service.getPublishedArticles());
    }

    @Test
    void getArticleByTitleWithoutFault() {
        ArticleRepository repository = Mockito.mock(ArticleRepository.class);
        UserRepository userRepository = Mockito.mock(UserRepository.class);
        ArticleFactory factory = new ArticleFactory();
        ArticleService service = new ArticleService(repository,userRepository,factory);
        Article article = factory.generatenewArticle("Test", Category.JAVA,"TestContent","TestResume");
        Article article2 = factory.generatenewArticle("AutreTest", Category.JAVA,"TestContent2","TestResume2");
        article.setStatus(StatusArticle.PUBLISHED);
        article2.setStatus(StatusArticle.PUBLISHED);
        List<Article> articles = new ArrayList<>(List.of(article, article2));
        List<Article> expected = new ArrayList<>(List.of(article));
        Mockito.when(repository.findAll()).thenReturn(articles);

        Assertions.assertEquals(expected,service.getArticleByTitle("Test"));
    }

    @Test
    void getArticleByTitleWithAFault() {
        ArticleRepository repository = Mockito.mock(ArticleRepository.class);
        UserRepository userRepository = Mockito.mock(UserRepository.class);
        ArticleFactory factory = new ArticleFactory();
        ArticleService service = new ArticleService(repository,userRepository,factory);
        Article article = factory.generatenewArticle("Test", Category.JAVA,"TestContent","TestResume");
        Article article2 = factory.generatenewArticle("AutreTest", Category.JAVA,"TestContent2","TestResume2");
        article.setStatus(StatusArticle.PUBLISHED);
        article2.setStatus(StatusArticle.PUBLISHED);
        List<Article> articles = new ArrayList<>(List.of(article, article2));
        List<Article> expected = new ArrayList<>(List.of(article));
        Mockito.when(repository.findAll()).thenReturn(articles);

        Assertions.assertEquals(expected,service.getArticleByTitle("Tes"));
    }

    @Test
    void getAnArticleById() {
        ArticleRepository repository = Mockito.mock(ArticleRepository.class);
        UserRepository userRepository = Mockito.mock(UserRepository.class);
        ArticleFactory factory = new ArticleFactory();
        ArticleService service = new ArticleService(repository,userRepository,factory);
        Article article = factory.generatenewArticle("Test1", Category.JAVA,"TestContent","TestResume");
        Article article2 = factory.generatenewArticle("Test2", Category.JAVA,"TestContent2","TestResume2");
        List<Article> Articles = new ArrayList<>(List.of(article, article2));
        Mockito.when(repository.findById("3")).thenReturn(Optional.empty());
        Optional<Article> articleEmpty= service.getAnArticleById("3");
        Assertions.assertTrue(articleEmpty.isEmpty());
    }

    @Test
    void getArticlesByCategorie() {
        ArticleRepository repository = Mockito.mock(ArticleRepository.class);
        UserRepository userRepository = Mockito.mock(UserRepository.class);
        ArticleFactory factory = new ArticleFactory();
        ArticleService service = new ArticleService(repository,userRepository,factory);
        Article article = factory.generatenewArticle("Test", Category.JAVA,"TestContent","TestResume");
        Article article2 = factory.generatenewArticle("Autretest", Category.JAVA,"TestContent2","TestResume2");
        Article article3 = factory.generatenewArticle("AnotherTest", Category.PYTHON,"TestContent2","TestResume2");
        article.setStatus(StatusArticle.PUBLISHED);
        article2.setStatus(StatusArticle.PUBLISHED);
        article3.setStatus(StatusArticle.PUBLISHED);
        List<Article> articles = new ArrayList<>(List.of(article, article2, article3));
        List<Article> expected = new ArrayList<>(List.of(article, article2));
        Mockito.when(repository.findAll()).thenReturn(articles);

        Assertions.assertEquals(expected,service.getArticlesByCategorie(Category.JAVA));
    }
}