package com.zenika.academy.blog.Service;

import com.zenika.academy.blog.Repository.Domain.User;
import com.zenika.academy.blog.Repository.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class UserServiceTest {

    @Test
    void getAUser() {
        UserRepository repository = Mockito.mock(UserRepository.class);
        UserService service = new UserService(repository);
        User user = new User("1", "PLOP", "plop@plop", "plopi", null);
        User user2 = new User("2", "PLOP2", "plop2@plop", "plopi2", null);
        List<User> users = new ArrayList<>(List.of(user, user2));
        Mockito.when(repository.findById("3")).thenReturn(Optional.empty());
        Optional<User> userEmpty= service.getAUser("3");
        Assertions.assertTrue(userEmpty.isEmpty());
    }

    @Test
    void getAllUsers() {

        UserRepository repository = Mockito.mock(UserRepository.class);
        UserService service = new UserService(repository);
        User user = new User("1", "PLOP", "plop@plop", "plopi", null);
        User user2 = new User("2", "PLOP2", "plop2@plop", "plopi2", null);
        List<User> users = new ArrayList<>(List.of(user, user2));
        Mockito.when(repository.findAll()).thenReturn(users);
        Assertions.assertEquals(users, service.getAllUsers());
    }
}