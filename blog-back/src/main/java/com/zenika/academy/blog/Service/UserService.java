package com.zenika.academy.blog.Service;

import com.zenika.academy.blog.Controller.Representation.UserRepresentation;
import com.zenika.academy.blog.Controller.Representation.UserRepresentationFullInfo;
import com.zenika.academy.blog.Repository.Domain.Article;
import com.zenika.academy.blog.Repository.Domain.User;
import com.zenika.academy.blog.Repository.UserRepository;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class UserService {

    private UserRepository userRepository;

    public UserService(UserRepository userRepository) {

        this.userRepository = userRepository;
    }

    public User addNewUser(UserRepresentation userRepresentation) {
        String id = UUID.randomUUID().toString();
        User user = new User(id, userRepresentation.getPseudo(), userRepresentation.getMail(), userRepresentation.getPassword(), null);
        userRepository.save(user);
        return user;
    }

    public void deleteUser(String id) {
        userRepository.deleteById(id);
    }

    public Optional<UserRepresentationFullInfo> getOneUserFullInfo(String idUser) {
        Optional<User> user = getAUser(idUser);
        return user.map(user1 -> new UserRepresentationFullInfo(user1.getId(), user1.getPseudo(), user1.getMail(), user1.getPassword(), user1.getArticles()));
    }

    public Optional<User> getAUser(String idUser) {
        Optional<User> user = userRepository.findById(idUser);
        return user;
    }

    public List<Article> getArticlesforAUser(String idUser) {
        List<Article> ArticlesOfUser = new ArrayList<>();
        Optional<User> user = userRepository.findById(idUser);
        user.ifPresent(user1 -> {
            for (Article article : user1.getArticles()) {
                ArticlesOfUser.add(article);
            }
        });
        return ArticlesOfUser;
    }

    public List<User> getAllUsers() {
        List<User> users = new ArrayList<>();
        for (User user : userRepository.findAll()) {
            users.add(user);
        }
        return users;
    }
}
