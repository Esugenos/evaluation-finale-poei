package com.zenika.academy.blog.Controller.Representation;

import com.zenika.academy.blog.Repository.Domain.Enum.StatusArticle;

public class ChangeStatusRepresentation {

    private String id;
    private StatusArticle status;

    public ChangeStatusRepresentation(){}

    public ChangeStatusRepresentation(String id, StatusArticle status){

        this.id = id;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public StatusArticle getStatus() {
        return status;
    }

    public void setStatus(StatusArticle status) {
        this.status = status;
    }
}
