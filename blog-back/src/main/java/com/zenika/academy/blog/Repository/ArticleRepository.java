package com.zenika.academy.blog.Repository;

import com.zenika.academy.blog.Repository.Domain.Article;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ArticleRepository extends CrudRepository<Article,String> {
}
