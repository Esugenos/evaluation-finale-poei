package com.zenika.academy.blog.Service;

import com.zenika.academy.blog.Controller.Representation.*;
import com.zenika.academy.blog.Repository.ArticleRepository;
import com.zenika.academy.blog.Repository.Domain.Article;
import com.zenika.academy.blog.Repository.Domain.Enum.Category;
import com.zenika.academy.blog.Repository.Domain.Enum.StatusArticle;
import com.zenika.academy.blog.Repository.UserRepository;
import com.zenika.academy.blog.Service.Factory.ArticleFactory;
import org.apache.commons.text.similarity.LevenshteinDistance;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class ArticleService {
    private ArticleRepository articleRepository;
    private UserRepository userRepository;
    private ArticleFactory articleFactory;
    private LevenshteinDistance distL;
    private Article article;

    public ArticleService(ArticleRepository articleRepository, UserRepository userRepository, ArticleFactory articleFactory) {
        this.articleRepository = articleRepository;
        this.userRepository = userRepository;
        this.articleFactory = articleFactory;

        this.distL = new LevenshteinDistance();
    }

    public ArticleRepresentationFullInfo addNewArticle(ArticleRepresentationTitleCategoryResumeContent articleRepresentation) {
        ArticleRepresentationFullInfo response;
        article = articleRepository.save(articleFactory.generatenewArticle(articleRepresentation.getTitle(),
                articleRepresentation.getCategory(), articleRepresentation.getContent(), articleRepresentation.getResume()));
        addArticleToUser(article.getId(), "UNKNOWN");
        response = new ArticleRepresentationFullInfo(article.getId(), article.getTitle(), article.getCategory(), article.getStatus(),
                article.getResume(), article.getContent());
        return response;
    }

    public Optional<Article> updateArticle(ChangeContentRepresentation newContent) {
        Optional<Article> article = articleRepository.findById(newContent.getId());
        article.ifPresent(article1 -> article1.setContent(newContent.getContent()));
        articleRepository.save(article.get());
        return article;
    }

    public void publishArticle(ChangeStatusRepresentation newStatus) {
        Optional<Article> article = articleRepository.findById(newStatus.getId());
        article.ifPresent(article1 -> article1.setStatus(newStatus.getStatus()));
        articleRepository.save(article.get());
    }

    public void addArticleToUser(String idArticle, String idUser) {

        userRepository.findById(idUser).ifPresent(user -> {
            articleRepository.findById(idArticle).ifPresent(article1 -> user.addArticleToUser(article1));
            userRepository.save(user);
        });
    }

    public List<Article> getAllArticles() {
        List<Article> articles = new ArrayList<>();
        for (Article article : articleRepository.findAll()) {
            articles.add(article);
        }
        return articles;
    }

    public List<Article> getDraftArticles() {
        List<Article> articlesDraft = new ArrayList<>();
        for (Article article : articleRepository.findAll()) {
            if (article.getStatus() == StatusArticle.DRAFT) {
                articlesDraft.add(article);
            }
        }
        return articlesDraft;
    }

    public List<Article> getDraftArticlesById(String id) {
        List<Article> articlesDraft = getDraftArticles();
        List<Article> articleDraft = new ArrayList<>();
        for (Article article : articlesDraft) {
            if (article.getId().equals(id)) {
                articleDraft.add(article);
            }
        }
        return articleDraft;
    }

    public List<Article> getArticlesByCategorie(Category category) {
        List<Article> articles = new ArrayList<>();
        for (Article article : articleRepository.findAll()) {
            if (article.getStatus() == StatusArticle.PUBLISHED) {
                switch (category) {
                    case JAVA:
                        if (article.getCategory() == Category.JAVA) {
                            articles.add(article);
                        }
                        break;
                    case CSHARP:
                        if (article.getCategory() == Category.CSHARP) {
                            articles.add(article);
                        }
                        break;
                    case DEVOPS:
                        if (article.getCategory() == Category.DEVOPS) {
                            articles.add(article);
                        }
                        break;
                    case PYTHON:
                        if (article.getCategory() == Category.PYTHON) {
                            articles.add(article);
                        }
                        break;
                    case PEOPLESOFT:
                        if (article.getCategory() == Category.PEOPLESOFT) {
                            articles.add(article);
                        }
                        break;
                }
            }
        }
        return articles;
    }

    public List<Article> getPublishedArticles() {
        List<Article> articlesPublished = new ArrayList<>();
        for (Article article : articleRepository.findAll()) {
            if (article.getStatus() == StatusArticle.PUBLISHED) {
                articlesPublished.add(article);
            }
        }
        return articlesPublished;
    }

    public String deleteArticle(String id) {
        articleRepository.deleteById(id);
        return "Article supprimé";
    }

    public List<Article> getArticleByTitle(String search) {
        List<Article> articleSearch = new ArrayList<>();
        List<Article> articleSearch2 = new ArrayList<>();
        List<Article> articles = new ArrayList<>();
        for (Article article : articleRepository.findAll()) {
            if (article.getStatus() == StatusArticle.PUBLISHED) {
                String title = article.getTitle();
                final Integer distanceWithCorrectTitle = distL.apply(search.toLowerCase(), title.toLowerCase());
                if (distanceWithCorrectTitle < 2) {
                    if (distanceWithCorrectTitle == 0) {
                        articleSearch.add(article);
                        articles = articleSearch;
                    } else {
                        articleSearch2.add(article);
                        articles = articleSearch2;
                    }
                }
            }
        }
        return articles;
    }

    public Optional<Article> getAnArticleById(String id) {
        return articleRepository.findById(id);
    }
}
