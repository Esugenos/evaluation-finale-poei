package com.zenika.academy.blog.Controller.Representation;

public class ArticleRepresentationTitleResume {

    private String title;
    private String resume;

    public ArticleRepresentationTitleResume(){}

    public ArticleRepresentationTitleResume(String title, String resume){

        this.title = title;
        this.resume = resume;
    }

    public String getTitle() {
        return title;
    }

    public String getResume() {
        return resume;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setResume(String resume) {
        this.resume = resume;
    }
}
