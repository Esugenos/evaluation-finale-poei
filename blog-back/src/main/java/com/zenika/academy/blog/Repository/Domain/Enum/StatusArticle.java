package com.zenika.academy.blog.Repository.Domain.Enum;

public enum StatusArticle {
    DRAFT,
    PUBLISHED
}
