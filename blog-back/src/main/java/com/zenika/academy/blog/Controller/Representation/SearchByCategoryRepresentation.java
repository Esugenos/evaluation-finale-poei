package com.zenika.academy.blog.Controller.Representation;

import com.zenika.academy.blog.Repository.Domain.Enum.Category;

public class SearchByCategoryRepresentation {

    Category category;

    public SearchByCategoryRepresentation(){}

    public SearchByCategoryRepresentation(Category category) {
        this.category = category;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
