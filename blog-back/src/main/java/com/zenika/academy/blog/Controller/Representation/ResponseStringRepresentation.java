package com.zenika.academy.blog.Controller.Representation;

public class ResponseStringRepresentation {
    String response;
    public ResponseStringRepresentation(){}


    public ResponseStringRepresentation(String response){
        this.response = response;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
