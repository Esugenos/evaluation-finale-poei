package com.zenika.academy.blog.Controller.Representation;

import com.zenika.academy.blog.Repository.Domain.Enum.Category;
import com.zenika.academy.blog.Repository.Domain.Enum.StatusArticle;

public class ArticleRepresentationFullInfo {

    private String id;
    private String title;
    private Category category;
    private StatusArticle status;
    private String resume;
    private String content;

    public ArticleRepresentationFullInfo() {
    }

    public ArticleRepresentationFullInfo(String id, String title, Category category, StatusArticle status, String resume, String content) {
        this.id = id;
        this.title = title;
        this.category = category;
        this.status = status;
        this.resume = resume;
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getResume() {
        return resume;
    }

    public void setResume(String resume) {
        this.resume = resume;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public StatusArticle getStatus() {
        return status;
    }

    public void setStatus(StatusArticle status) {
        this.status = status;
    }
}
