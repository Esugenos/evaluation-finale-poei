package com.zenika.academy.blog.Controller;

import com.zenika.academy.blog.Controller.Representation.*;
import com.zenika.academy.blog.Repository.Domain.Article;
import com.zenika.academy.blog.Repository.Domain.Enum.Category;
import com.zenika.academy.blog.Service.ArticleService;
import org.apache.coyote.Response;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/articles")
public class ArticleController {
    ArticleService service;

    public ArticleController(ArticleService service) {
        this.service = service;
    }

    @DeleteMapping("/delete/{id}")
    public String deleteAnArticleById(@PathVariable(value = "id") String id) {
        Optional<Article> deletedArticle = service.getAnArticleById(id);
        service.deleteArticle(id);
        return deletedArticle.map(article -> "Votre article " + article.getTitle() + " a bien été supprimé").orElse("Article introuvable");
    }

    @PostMapping("/create")
    public ArticleRepresentationFullInfo createNewArticle(@RequestBody ArticleRepresentationTitleCategoryResumeContent body) {
        ArticleRepresentationFullInfo newArticle= service.addNewArticle(body);
            return newArticle;
    }

    @PutMapping("/publish")
    public ResponseStringRepresentation changeStatus(@RequestBody ChangeStatusRepresentation body) {
        service.publishArticle(body);
        Article article = service.getAnArticleById(body.getId()).get();
        ResponseStringRepresentation response = new ResponseStringRepresentation("Changement de statut de l'article effectué pour l'article " + article.getTitle());
        return response;
    }

    @PutMapping("/updatecontent")
    public ResponseStringRepresentation changeContent(@RequestBody ChangeContentRepresentation body) {
        service.updateArticle(body);
        Article article = service.getAnArticleById(body.getId()).get();
        ResponseStringRepresentation response = new ResponseStringRepresentation("Changement de contenu effectué pour l'article " + article.getTitle());
        return response;
    }

    @GetMapping
    public List<ArticleRepresentationFullInfo> getAllArticles() {
        List<ArticleRepresentationFullInfo> articlesF = new ArrayList<>();
        for (Article article : service.getAllArticles()) {
            ArticleRepresentationFullInfo articleF = new ArticleRepresentationFullInfo(article.getId(), article.getTitle(),
                    article.getCategory(), article.getStatus(), article.getResume(), article.getContent());
            articlesF.add(articleF);
        }
        return articlesF;
    }

    @GetMapping("/draft")
    public List<ArticleRepresentationFullInfo> getDraftArticles() {
        List<ArticleRepresentationFullInfo> articlesF = new ArrayList<>();
        for (Article article : service.getDraftArticles()) {
            ArticleRepresentationFullInfo articleF = new ArticleRepresentationFullInfo(article.getId(), article.getTitle(),
                    article.getCategory(), article.getStatus(), article.getResume(), article.getContent());
            articlesF.add(articleF);
        }
        return articlesF;
    }

    @GetMapping("/published")
    public List<ArticleRepresentationFullInfo> getPublishedArticles() {
        List<ArticleRepresentationFullInfo> articlesF = new ArrayList<>();
        for (Article article : service.getPublishedArticles()) {
            ArticleRepresentationFullInfo articleF = new ArticleRepresentationFullInfo(article.getId(), article.getTitle(),
                    article.getCategory(), article.getStatus(), article.getResume(), article.getContent());
            articlesF.add(articleF);
        }
        return articlesF;
    }
    @GetMapping("/category/{category}")
    public List<ArticleRepresentationFullInfo> getArticlesByCategory(@PathVariable (value="category") Category category){
        List<ArticleRepresentationFullInfo> articles = new ArrayList<>();
        for (Article article : service.getArticlesByCategorie(category)) {
            ArticleRepresentationFullInfo articleF = new ArticleRepresentationFullInfo(article.getId(), article.getTitle(),
                    article.getCategory(), article.getStatus(), article.getResume(), article.getContent());
            articles.add(articleF);
        }
        return articles;
    }

    @GetMapping("/{id}")
    Article getAnArticleById(@PathVariable(value = "id") String id) {
        Optional<Article> article = this.service.getAnArticleById(id);
        return article.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Cet article n'existe pas."));
    }
    @GetMapping("/draft/{id}")
    List<Article> getADraftArticleById(@PathVariable(value = "id") String id) {
        List<Article> article = this.service.getDraftArticlesById(id);
        return article;
    }

    @GetMapping("/search/{search}")
    Object searchByTitle(@PathVariable(value = "search") String search) {
        Object response;
        List<Article> article = this.service.getArticleByTitle(search);
        if (article != null) {
            response = article;
        } else {
            response = new ResponseStatusException(HttpStatus.NOT_FOUND, "Cet article n'existe pas.");
        }
        return response;
    }

    @PutMapping("/jointouser")
    public void addArticleToUser(@RequestBody JoinArticleToUserBodyRepresentation body) {
        service.addArticleToUser(body.getIdArticle(), body.getIdUser());
    }
}
