package com.zenika.academy.blog.Repository.Domain;

import com.zenika.academy.blog.Repository.Domain.Enum.Category;
import com.zenika.academy.blog.Repository.Domain.Enum.StatusArticle;

import javax.persistence.*;

@Entity
@Table(name = "article")
public class Article {
    @Id
    private String id;
    private String title;
    @Enumerated(value = EnumType.STRING)
    private Category category;
    @Enumerated(value = EnumType.STRING)
    private StatusArticle status;
    private String content;
    private String resume;


    public Article() {
    }


    public Article(String id, String title, Category category, StatusArticle status, String content, String resume) {
        this.id = id;
        this.title = title;
        this.category = category;
        this.status = status;
        this.content = content;
        this.resume = resume;

    }


    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public String getResume() {
        return resume;
    }

    public StatusArticle getStatus() {
        return status;
    }

    public void setStatus(StatusArticle status) {
        this.status = status;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setResume(String resume) {
        this.resume = resume;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
