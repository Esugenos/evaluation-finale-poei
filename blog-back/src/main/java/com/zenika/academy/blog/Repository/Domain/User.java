package com.zenika.academy.blog.Repository.Domain;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "user_account")
public class User {

    @Id
    private String id;
    private String pseudo;
    private String mail;
    private String password;
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_user")
    private List<Article> articles;

    public User(){}

    public User(String id, String pseudo, String mail,String password, List<Article>articles){

        this.id = id;
        this.pseudo = pseudo;
        this.mail = mail;
        this.password = password;
        this.articles = articles;
    }
    public void addArticleToUser(Article article){
        articles.add(article);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Article> getArticles() {
        return articles;
    }

    public void setArticles(List<Article> articles) {
        this.articles = articles;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
}
