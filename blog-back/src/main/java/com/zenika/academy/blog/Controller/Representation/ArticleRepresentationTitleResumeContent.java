package com.zenika.academy.blog.Controller.Representation;

public class ArticleRepresentationTitleResumeContent {

    private String title;
    private String resume;
    private String content;

    public ArticleRepresentationTitleResumeContent(){}

    public ArticleRepresentationTitleResumeContent(String title, String resume, String content){

        this.title = title;
        this.resume = resume;
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getResume() {
        return resume;
    }

    public void setResume(String resume) {
        this.resume = resume;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
