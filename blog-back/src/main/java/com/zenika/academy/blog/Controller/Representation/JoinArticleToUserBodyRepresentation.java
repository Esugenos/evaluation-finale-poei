package com.zenika.academy.blog.Controller.Representation;

public class JoinArticleToUserBodyRepresentation {
    private String idUser;
    private String idArticle;

    public JoinArticleToUserBodyRepresentation(){}

    public JoinArticleToUserBodyRepresentation(String idUser, String idArticle){

        this.idUser = idUser;
        this.idArticle = idArticle;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getIdArticle() {
        return idArticle;
    }

    public void setIdArticle(String idArticle) {
        this.idArticle = idArticle;
    }
}
