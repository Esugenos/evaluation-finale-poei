package com.zenika.academy.blog.Service.Factory;

import com.zenika.academy.blog.Repository.Domain.Article;
import com.zenika.academy.blog.Repository.Domain.Enum.Category;
import com.zenika.academy.blog.Repository.Domain.Enum.StatusArticle;
import com.zenika.academy.blog.Service.ArticleService;
import com.zenika.academy.blog.Service.UserService;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class ArticleFactory {
    private UserService userService;
    private ArticleService articleService;

    public ArticleFactory() {

        this.userService = userService;
        this.articleService = articleService;
    }

    public Article generatenewArticle(String title, Category category, String content, String resume) {
        String id = UUID.randomUUID().toString();
        StatusArticle status = StatusArticle.DRAFT;
        Article article = new Article(id, title, category, status, content, resume);
        return article;
    }
}
