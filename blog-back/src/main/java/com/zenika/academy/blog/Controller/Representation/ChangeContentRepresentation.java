package com.zenika.academy.blog.Controller.Representation;

import com.zenika.academy.blog.Repository.Domain.Enum.StatusArticle;

public class ChangeContentRepresentation {

    private String id;
    private String content;

    public ChangeContentRepresentation(){}

    public ChangeContentRepresentation(String id, String content){

        this.id = id;
        this.content = content;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
