package com.zenika.academy.blog.Controller;

import com.zenika.academy.blog.Controller.Representation.UserRepresentation;
import com.zenika.academy.blog.Controller.Representation.UserRepresentationFullInfo;
import com.zenika.academy.blog.Repository.Domain.Article;
import com.zenika.academy.blog.Repository.Domain.User;
import com.zenika.academy.blog.Service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/users")
public class UserController {

    private UserService service;

    public UserController(UserService service) {

        this.service = service;
    }

    @DeleteMapping("/delete/{id}")
    public String deleteAUserById(@PathVariable(value = "id") String id) {
        Optional<User> deletedUser = service.getAUser(id);
        User inc = deletedUser.get();
        if (!inc.getId().equals("UNKNOWN")) {
            service.deleteUser(id);
            return deletedUser.map(user -> "Le compte au nom de " + user.getPseudo() + " a bien été supprimé").orElse("Compte introuvable");
        }else{
            return "Vous n'êtes pas autoriser à supprimer cet utilisateur";
        }
    }

    @PostMapping("/create")
    public UserRepresentationFullInfo createNewUser(@RequestBody UserRepresentation body) {
        User newUser = service.addNewUser(body);
        UserRepresentationFullInfo response = new UserRepresentationFullInfo(newUser.getId(), newUser.getPseudo(),
                newUser.getMail(), newUser.getPassword(), newUser.getArticles());
        return response;
    }

    @GetMapping
    public List<UserRepresentationFullInfo> getAllUser() {
        List<UserRepresentationFullInfo> users = new ArrayList<>();
        for (User user : service.getAllUsers()) {
            UserRepresentationFullInfo userR = new UserRepresentationFullInfo(user.getId(),user.getPseudo(), user.getMail(), user.getPassword(), user.getArticles());
            users.add(userR);
        }
        return users;
    }

    @GetMapping("/{id}")
    public UserRepresentationFullInfo getAUser(@PathVariable(value = "id") String id) {
        Optional<UserRepresentationFullInfo> user = this.service.getOneUserFullInfo(id);
        return user.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Compte introuvable"));
    }

    @GetMapping("/your_articles/{id}")
    public List<Article> getArticlesOfAUser(@PathVariable(value = "id") String idPlayer) {
        return service.getArticlesforAUser(idPlayer);
    }
}


