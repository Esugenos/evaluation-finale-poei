package com.zenika.academy.blog.Controller.Representation;

import com.zenika.academy.blog.Repository.Domain.Article;

import java.util.List;

public class UserRepresentationFullInfo {
    private String id;
    private String pseudo;
    private String mail;
    private String password;
    private List<Article> articles;

    public UserRepresentationFullInfo(){}

    public UserRepresentationFullInfo(String id,String pseudo, String mail,String password, List<Article> articles){
        this.id = id;
        this.pseudo = pseudo;
        this.mail = mail;
        this.password = password;
        this.articles = articles;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Article> getArticles() {
        return articles;
    }

    public void setArticles(List<Article> articles) {
        this.articles = articles;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
}
