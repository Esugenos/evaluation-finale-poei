package com.zenika.academy.blog.Repository.Domain.Enum;

public enum Category {
    JAVA,
    DEVOPS,
    PYTHON,
    CSHARP,
    PEOPLESOFT
}
