package com.zenika.academy.blog.Controller.Representation;

import com.zenika.academy.blog.Repository.Domain.Enum.Category;

public class ArticleRepresentationTitleCategoryResumeContent {

    private String title;
    private Category category;
    private String resume;
    private String content;

    public ArticleRepresentationTitleCategoryResumeContent(){}

    public ArticleRepresentationTitleCategoryResumeContent(String title, Category category,String resume, String content){

        this.title = title;
        this.category = category;
        this.resume = resume;
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getResume() {
        return resume;
    }

    public void setResume(String resume) {
        this.resume = resume;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
