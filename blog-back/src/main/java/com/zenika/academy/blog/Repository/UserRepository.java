package com.zenika.academy.blog.Repository;

import com.zenika.academy.blog.Repository.Domain.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User,String> {
}
