CREATE TABLE user_account(
id text not null PRIMARY KEY,
pseudo text not null,
mail text not null,
password text not null
);

INSERT INTO user_account VALUES ('UNKNOWN','Auteur inconnu','anonymous@anonymous' ,'0000');

CREATE TABLE article(
id text not null PRIMARY KEY,
title text not null,
category text not null,
status text not null,
content text not null,
resume text not null,
id_user text REFERENCES user_account(id)
)